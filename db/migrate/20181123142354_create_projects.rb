class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :title
      t.string :url
      t.string :vision
      t.string :description
      t.references :user, foreign_key: true
      t.boolean :public

      t.timestamps
    end
  end
end
