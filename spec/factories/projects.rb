FactoryBot.define do
  factory :project do
    title { "MyString" }
    vision { "MyString" }
    description { "MyString" }
    user { nil }
    public { false }
  end
end
