require 'rails_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @valid_attributes = {
      username: "Test User",
      email: "testuser@worldofmind.org",
      password: "test42"
    }
  end

  it "is valid with valid attributes" do
    user = User.new(@valid_attributes)

    expect(user).to be_valid
  end

  it "is not valid without a username" do
    @valid_attributes[:username] = nil
    user = User.new(@valid_attributes)

    expect(user).to_not be_valid
  end

  it "is not valid without a email" do
    @valid_attributes[:email] = nil
    user = User.new(@valid_attributes)

    expect(user).to_not be_valid
  end

  it "is not valid without a password" do
    @valid_attributes[:password] = nil
    user = User.new(@valid_attributes)

    expect(user).to_not be_valid
  end
end
